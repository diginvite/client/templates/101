import React, { Component } from "react";
import axios from "axios";
import moment from "moment";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Helmet } from "react-helmet";

import {
  Preload,
  Header,
  Cover,
  Couple,
  Story,
  Quote,
  Gallery,
  Video,
  Form,
  Event,
  Footer,
  Modal,
} from "../components";
import { apiUrl } from "../actions/config";
import "../App.css";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      order: null,
      invite: null,
      modal: true,
      music: false,
    };
  }

  componentDidMount = () => {
    var url_string = document.URL;
    var url = new URL(url_string);
    var hostname = url.hostname;
    var hostnames = hostname.split(".");
    var param = url.searchParams.get("invitation");

    axios
      .get(`${apiUrl}/findOrder?domain=${hostnames[0]}&invitation=${param}`)
      .then((response) => {
        this.setState({
          order: response.data.order,
          invite: response.data.invitation,
        });
      })
      .catch((error) => {
        throw error;
      });
  };

  onAction = (event) => {
    const startDate = moment(event.startDate).format("YYYYMMDD");
    const startTime = moment(event.startDate).format("HHmmss");
    const endDate = moment(event.endDate).format("YYYYMMDD");
    const endTime = moment(event.endDate).format("HHmmss");
    const location = event.address;
    const couple = `${this.state.order.detail.couples[0].fullName} dan ${this.state.order.detail.couples[1].fullName}`;

    var url = `https://calendar.google.com/calendar/r/eventedit?text=${this.state.order.name}&dates=${startDate}T${startTime}/${endDate}T${endTime}&ctz=Asia/Jakarta&details=Merupakan suatu kehormatan dan kebahagiaan bagi kami apabila Bapak/Ibu/Saudara/Saudari berkenan hadir untuk memberikan doa restu kepada kami.<br><br>Terima kasih,<br><br>${couple}&location=${location}&pli=1&amp;uid=1521339627addtocalendar&sf=true&amp;output=xml`;
    var win = window.open(url, "_blank");
    win.focus();
  };

  onSubmit = (formInput) => {
    const data = {
      name: formInput.name,
      company: formInput.form,
      attend: formInput.attend,
      person: formInput.person,
      wish: formInput.wish,
    };
    axios
      .post(`${apiUrl}/insertOneWish/${this.state.order.id}`, data)
      .then((response) => {
        this.setState({
          order: response.data,
          name: "",
          company: "",
          wish: "",
        });
        toast.success("Data saved", {
          position: toast.POSITION.TOP_RIGHT,
        });
      })
      .catch((error) => {
        throw error;
      });
  };

  onTogglePlay(param) {
    var x = document.getElementById("myAudio");
    if (param === "play") {
      x.play();
    } else {
      x.pause();
    }
    this.setState({ music: !this.state.music });
  }

  render() {
    const songs =
      this.state.order &&
      this.state.order.detail.songs.map((song) => {
        if (song.isActive) {
          return song;
        } else {
          return null;
        }
      });
    if (this.state.order === null) {
      return <Preload />;
    }
    return (
      <>
        <div className="application">
          <Helmet>
            <meta charSet="utf-8" />
            <title>{this.state.order.name} - Diginvite</title>
          </Helmet>
        </div>
        <div className="page-wrapper flower-fixed-body">
          {this.state.order.detail.couples.length > 0 && (
            <Header data={this.state.order} />
          )}
          {this.state.order.detail.couples.length > 0 && (
            <Cover data={this.state.order} />
          )}
          {this.state.order.detail.couples.length > 0 && (
            <Couple data={this.state.order} />
          )}
          {this.state.order.detail.events.length > 0 && (
            <Event
              data={this.state.order}
              onAction={(flag) => this.onAction(flag)}
            />
          )}
          {this.state.order.detail.stories.length > 0 && (
            <Story data={this.state.order} />
          )}
          {this.state.order.detail.quotes.length > 0 && (
            <Quote data={this.state.order} />
          )}
          {this.state.order.detail.images.length > 0 && (
            <Gallery data={this.state.order} />
          )}
          {this.state.order.detail.videos.length > 0 && (
            <Video data={this.state.order} />
          )}
          <Form
            data={this.state.order}
            name={this.state.name}
            company={this.state.company}
            attend={this.state.attend}
            person={this.state.person}
            wish={this.state.wish}
            onChange={(e) => this.onChange(e)}
            onSubmit={(e) => this.onSubmit(e)}
          />
          {/* <Gift data={this.state.order} /> */}
          <Footer data={this.state.order} />
          <ToastContainer />
        </div>
        <audio loop="" preload="auto" id="myAudio" src={songs[0].url} />
        {this.state.invite !== "null" ? (
          <Modal
            visible={this.state.modal}
            invite={this.state.invite}
            order={this.state.order}
            onClose={() => {
              this.setState({ modal: false });
            }}
          />
        ) : null}
        {!this.state.music ? (
          <a
            href="#home"
            class="music"
            onClick={() => this.onTogglePlay("play")}
          >
            <i class="ti-music"></i>
          </a>
        ) : (
          <a
            href="#home"
            class="music"
            onClick={() => this.onTogglePlay("pause")}
          >
            <i class="ti-volume"></i>
          </a>
        )}
      </>
    );
  }
}

export default Home;
