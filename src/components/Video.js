import React from "react";
import { Player } from "video-react";
import "video-react/dist/video-react.css"; // import css

export default ({ data }) => {
	return (
		<section className="gallery-section section-padding p-t-0">
			<div className="container">
				<div className="row">
					<div className="col col-lg-12 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
						<div className="section-title">
							<h2>Video Kami</h2>
							{/* <p>Recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat</p> */}
						</div>
					</div>
				</div>

				<div className="row">
					{data.detail.videos.map((video) => {
						return (
							<div className="col col-xs-12">
								<div className="gallery-container gallery-fancybox masonry-gallery">
									<Player playsInline poster={video.thumbnailUrl} src={video.url} />
								</div>
							</div>
						);
					})}
				</div>
			</div>
		</section>
	);
};
