import React from "react";

const Footer = (props) => {
  return (
    <footer className="site-footer footer-style-1">
      <div className="inner">
        {/* <div className="couple-pic">
                    <img src="http://themegeniuslab.com/html/jolly/assets/images/footer-couple-pic.jpg" alt=""/>
                </div> */}
        <h2>Terima kasih</h2>
        {/* <ul className="social-links">
                    <li><a href="#"><i className="ti-facebook"></i></a></li>
                    <li><a href="#"><i className="ti-twitter-alt"></i></a></li>
                    <li><a href="#"><i className="ti-linkedin"></i></a></li>
                    <li><a href="#"><i className="ti-pinterest"></i></a></li>
                </ul> */}
        <p>
          Copyright 2019, Made with{" "}
          <span role="img" aria-label="love">
            ❤️
          </span>{" "}
          by{" "}
          <a href="https://diginvite.com" target="blank">
            Diginvite
          </a>
        </p>
      </div>
    </footer>
  );
};

export default Footer;
