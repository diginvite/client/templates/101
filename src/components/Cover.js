import React from "react";
import ImageGallery from "react-image-gallery";
import Countdown from "react-countdown-now";
import Moment from "react-moment";
// import { maxHeaderSize } from "http";

const renderer = ({ days, hours, minutes, seconds }) => {
  return (
    <>
      <div className="box">
        <div>{days}</div>
        <span>Hari</span>
      </div>
      <div className="box">
        <div>{hours}</div>
        <span>Jam</span>
      </div>
      <div className="box">
        <div>{minutes}</div>
        <span>Menit</span>
      </div>
      <div className="box">
        <div>{seconds}</div>
        <span>Detik</span>
      </div>
    </>
  );
};

const Cover = (props) => {
  var event = null;
  var images = [];

  for (let i = 0; i < props.data.detail.events.length; i++) {
    if (props.data.detail.events[i].isCoundown || i === 0) {
      event = props.data.detail.events[i];
    }
  }

  for (let i = 0; i < props.data.detail.images.length; i++) {
    if (props.data.detail.images[i].isCover) {
      const image = {
        original: props.data.detail.images[i].url,
        thumbnail: props.data.detail.images[i].url,
      };
      images.push(image);
    }
  }

  return (
    <>
      <section className="hero-slider hero-style-1">
        <div className="swiper-container">
          <div className="slide-main-text">
            <div className="container">
              <div className="slide-title">
                <h2>
                  {props.data.detail.couples[0].nickName} &{" "}
                  {props.data.detail.couples[1].nickName}
                </h2>
              </div>
              <div className="wedding-date">
                <span>
                  <Moment format="DD MMM. YYYY HH:mm:SS">
                    {event && event.startDate}
                  </Moment>
                </span>
              </div>
              <div className="clearfix"></div>
              <div className="count-down-clock">
                <div id="clock">
                  <Countdown
                    date={event && event.startDate}
                    renderer={renderer}
                  />
                </div>
              </div>
            </div>
            <div className="pattern">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
          <div className="swiper-wrapper">
            {/* <div className="swiper-slide">
                        <div className="slide-inner slide-bg-image" data-background="http://themegeniuslab.com/html/jolly/assets/images/slider/slide-1.jpg"></div> 
                    </div> 

                    <div className="swiper-slide">
                        <div className="slide-inner slide-bg-image" data-background="http://themegeniuslab.com/html/jolly/assets/images/slider/slide-2.jpg"></div>
                    </div> 

                    <div className="swiper-slide">
                        <div className="slide-inner slide-bg-image" data-background="http://themegeniuslab.com/html/jolly/assets/images/slider/slide-3.jpg"></div>
                    </div> */}
            <ImageGallery
              useBrowserFullscreen={true}
              items={images}
              showFullscreenButton={false}
              showPlayButton={false}
              disableArrowKeys={false}
              showNav={false}
              disableSwipe={false}
              showBullets={false}
              showThumbnails={false}
              autoPlay
              slideInterval={5000}
              slideDuration={2000}
              originalClass={{ maxHeaderSize: "100%" }}
            />
          </div>
          {/* <div className="swiper-pagination"></div> */}
          {/* <div className="swiper-button-next"></div> */}
          {/* <div className="swiper-button-prev"></div> */}
        </div>
      </section>
    </>
  );
};

export default Cover;
