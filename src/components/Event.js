import React from "react";
import Moment from "react-moment";

const Event = (props) => {
  return (
    <section
      className="event-section section-padding p-t-0"
      style={{ backgroundImage: "image.jpg" }}
    >
      <div className="top-area">
        <h2>Acara Kami</h2>
        {/* <p className="date">14 Feb 2020</p> */}
      </div>
      <div className="container">
        <div className="row">
          <div className="col col-xs-12">
            <div className="event-grids clearfix">
              {props.data &&
                props.data.detail.events.map((data, i) => {
                  return (
                    <div className="grid">
                      <h3>{data.name}</h3>
                      <p>
                        <Moment format="DD MMMM, YYYY">{data.startDate}</Moment>
                        &nbsp;at&nbsp;
                        <Moment format="HH:mm">{data.startDate}</Moment>
                        &nbsp; - &nbsp;
                        <Moment format="HH:mm">{data.endDate}</Moment>
                        {/* 13 February, 2020 at 9AM - 11 AM */}
                      </p>
                      <p>{data.address}</p>
                      <p>
                        - <i>{data.note}</i> -
                      </p>
                      {/* <p className="phone">Ph: 0123456634654</p> */}
                      <a
                        href="#here"
                        className="location popup-gmaps"
                        onClick={() => props.onAction(data)}
                      >
                        Add to calendar
                      </a>
                      <br />
                      <br />
                      <a
                        href={`http://maps.google.com/maps?z=12&t=m&q=loc:${data.lat}+${data.lng}`}
                        className="location popup-gmaps"
                        target="blank"
                      >
                        See Location
                      </a>
                      <br />
                      <div></div>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Event;
