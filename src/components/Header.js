import React from "react";

const Header = ({ data }) => {
  return (
    <header id="header" className="site-header header-style-1">
      <div className="topbar">
        <div className="container">
          <div className="row">
            <div className="col col-xs-12">
              <div className="site-logo">
                <h1>
                  {data.detail.couples[0].nickName} &{" "}
                  {data.detail.couples[1].nickName}
                </h1>
                <span>Menikah</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
