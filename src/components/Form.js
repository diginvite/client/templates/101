import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { ToastContainer, toast } from "react-toastify";

const settings = {
  dots: false,
  infinite: true,
  // speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  speed: 3000,
  autoplaySpeed: 2000,
};

const Form = (props) => {
  const [formInput, setFormInput] = useState({
    name: "",
    form: "",
    person: 1,
    attend: 1,
    wish: "",
  });

  const [wishes, setWishes] = useState([]);

  useEffect(() => {
    if (props.data) {
      const newWishes = props.data.detail.wishes.map((wish) => {
        return (
          <div className="row">
            <div className="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
              <div className="text-center">
                <p>
                  <i>{wish.wish}</i>
                </p>
                <b>
                  <i>{wish.name}</i>
                </b>
                <p>({wish.company})</p>
              </div>
            </div>
          </div>
        );
      });
      setWishes(newWishes);
    }
  }, [props]);

  const onChange = (e) => {
    const { name, value } = e.target;
    const newInput = { ...formInput, [name]: value };
    setFormInput(newInput);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (formInput.name === "" || formInput.wish === "") {
      return toast.error("Name and wish are required", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    props.onSubmit(formInput);
    setFormInput({
      name: "",
      form: "",
      person: 1,
      attend: 1,
      wish: "",
    });
  };

  return (
    <section className="contact-section section-padding p-t-0" id="rsvp">
      <div className="container">
        <div className="row">
          <div className="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
            <div className="section-title">
              <h2>Buku Tamu</h2>
              {/* <p>Recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat</p> */}
            </div>
          </div>
        </div>
        <Slider {...settings}>{wishes}</Slider>
        <br />
        <div className="row">
          <div className="col col-lg-10 col-lg-offset-1">
            <div className="contact-form" onSubmit={onSubmit}>
              <form
                id="rsvp-form"
                className="form validate-rsvp-form row"
                method="post"
              >
                <div className="col col-sm-6">
                  <label>Nama</label>
                  <input
                    type="text"
                    name="name"
                    className="form-control"
                    placeholder="Nama*"
                    value={formInput.name}
                    onChange={onChange}
                  />
                </div>
                <div className="col col-sm-6">
                  <label>Siapanya siapa?</label>
                  <input
                    type="text"
                    name="form"
                    className="form-control"
                    placeholder="Co. rekan kerja Jhon"
                    value={formInput.form}
                    onChange={onChange}
                  />
                </div>
                <div className="col col-sm-6">
                  <label>Jumlah tamu</label>
                  <select
                    className="form-control"
                    name="person"
                    value={formInput.person}
                    onChange={onChange}
                  >
                    <option disabled selected>
                      Jumlah tamu*
                    </option>
                    <option value="1">1 Orang</option>
                    <option value="2">2 Orang</option>
                    {/* <option>3</option>
                                        <option>4</option> */}
                  </select>
                </div>
                <div className="col col-sm-6">
                  <label>Hadir</label>
                  <select
                    className="form-control"
                    name="attend"
                    value={formInput.attend}
                    onChange={onChange}
                  >
                    <option value="1">Hadir</option>
                    <option value="2">Tentatif</option>
                    <option value="0">Tidak hadir</option>
                  </select>
                </div>
                <div className="col col-sm-12">
                  <label>Ucapan & doa</label>
                  <textarea
                    className="form-control"
                    name="wish"
                    placeholder="Ucapan dan doa*"
                    value={formInput.wish}
                    onChange={onChange}
                  ></textarea>
                </div>
                <div className="col col-sm-12 submit-btn">
                  <button type="submit" className="theme-btn">
                    Kirim
                  </button>
                  <div id="loader">
                    <i className="ti-reload"></i>
                  </div>
                </div>
                <div className="col col-md-12 success-error-message">
                  <div id="success">Thank you</div>
                  <div id="error">
                    {" "}
                    Error occurred while sending email. Please try again later.{" "}
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <ToastContainer />
    </section>
  );
};

export default Form;
