import React from "react";

const Couple = (props) => {
  return (
    <section className="couple-section section-padding p-t-0">
      <div className="container">
        <div className="row">
          <div className="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
            <div className="section-title">
              <h2>Pasangan Bahagia</h2>
              {/* <p>Recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat</p> */}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col col-lg-10 col-lg-offset-1">
            <div className="couple-area">
              {props.data.detail.couples.map((data, i) => {
                return (
                  <div
                    className={
                      i === 0
                        ? "couple-row clearfix"
                        : "couple-row last-row  clearfix"
                    }
                  >
                    {i === 0 ? (
                      <div className="img-holder">
                        <img src={data.images[0].url} alt="couple_image" />
                      </div>
                    ) : null}
                    <div className="details">
                      <div className="inner">
                        <h2>{data.fullName}</h2>
                        {/* <h4>Hi Dear, I am Dorian Wiliam.</h4> */}
                        <p>{data.description}</p>
                        {/* <ul className="social-links">
                                                <li><a href="#"><i className="ti-facebook"></i></a></li>
                                                <li><a href="#"><i className="ti-twitter-alt"></i></a></li>
                                                <li><a href="#"><i className="ti-linkedin"></i></a></li>
                                                <li><a href="#"><i className="ti-pinterest"></i></a></li>
                                            </ul> */}
                      </div>
                    </div>
                    {i !== 0 ? (
                      <div className="img-holder">
                        <img src={data.images[0].url} alt="couple_image" />
                      </div>
                    ) : null}
                  </div>
                );
              })}
              {/* <div className="couple-row clearfix">
                                <div className="img-holder">
                                    <img src="http://themegeniuslab.com/html/jolly/assets/images/couple/img-1.jpg" alt/>
                                </div>
                                <div className="details">
                                    <div className="inner">
                                        <h2>Dorina Wiliam</h2>
                                        <h4>Hi Dear, I am Dorian Wiliam.</h4>
                                        <p>A collection of textile samples lay spread out on the table Samsa was a travelling salesman and above it there hung a picture that he had recently cut out of an illustrated magazine And housed in a nice, gilded frame. It showed a lady fitted out with a fur hat and fur boa who sat upright</p>
                                        <ul className="social-links">
                                            <li><a href="#"><i className="ti-facebook"></i></a></li>
                                            <li><a href="#"><i className="ti-twitter-alt"></i></a></li>
                                            <li><a href="#"><i className="ti-linkedin"></i></a></li>
                                            <li><a href="#"><i className="ti-pinterest"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="couple-row last-row clearfix">
                                <div className="details">
                                    <div className="inner">
                                        <h2>Blit Jhon</h2>
                                        <h4>Hi Dear, I am Blit Jhon.</h4>
                                        <p>A collection of textile samples lay spread out on the table Samsa was a travelling salesman and above it there hung a picture that he had recently cut out of an illustrated magazine And housed in a nice, gilded frame. It showed a lady fitted out with a fur hat and fur boa who sat upright</p>
                                        <ul className="social-links">
                                            <li><a href="#"><i className="ti-facebook"></i></a></li>
                                            <li><a href="#"><i className="ti-twitter-alt"></i></a></li>
                                            <li><a href="#"><i className="ti-linkedin"></i></a></li>
                                            <li><a href="#"><i className="ti-pinterest"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="img-holder">
                                    <img src="http://themegeniuslab.com/html/jolly/assets/images/couple/img-2.jpg" alt/>
                                </div>
                            </div> */}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Couple;
