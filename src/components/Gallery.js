import React from "react";
import Gallery from "react-grid-gallery";

const Gallery2 = (props) => {
  var images = [];
  if (props.data) {
    for (let i = 0; i < props.data.detail.images.length; i++) {
      if (props.data.detail.images[i].isGallery) {
        const image = {
          src: props.data.detail.images[i].url,
          thumbnail: props.data.detail.images[i].url,
        };
        images.push(image);
      }
    }
  }
  return (
    <section className="gallery-section section-padding p-t-0">
      <div className="container">
        <div className="row">
          <div className="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
            <div className="section-title">
              <h2>Galeri Kami</h2>
              {/* <p>Recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat</p> */}
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col col-xs-12">
            <div className="gallery-container gallery-fancybox masonry-gallery">
              <Gallery images={images} />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Gallery2;
