import React from "react";
// import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const Quote = (props) => {
  return (
    <section className="testimonials-section section-padding">
      <div className="container">
        <div className="row">
          <div className="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
            <div className="section-title">
              <h2>Kutipan Kami</h2>
              {/* <p>Recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat</p> */}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col col-xs-12">
            <div className="testimonial-grids clearfix">
              {props.data &&
                props.data.detail.quotes.map((data, i) => {
                  if (i <= 2) {
                    return (
                      <div className="grid">
                        <div className="quote">
                          <p>{data.description}</p>
                          <center>
                            <p>
                              - <i>{data.title}</i> -
                            </p>
                          </center>
                        </div>
                      </div>
                    );
                  } else {
                    return null;
                  }
                })}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Quote;
