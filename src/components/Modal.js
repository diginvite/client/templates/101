import React, { useEffect, useState } from "react";
import Modal from "react-awesome-modal";

const Modal2 = ({ invite, order, visible, onClose }) => {
  const [invitation, setInvitation] = useState([]);
  useEffect(() => {
    const newInvitation = order.detail.invitations.filter((invitation) => {
      return invitation.code === invite;
    });
    setInvitation(newInvitation);
  }, [invite, order]);

  return (
    <section className="text-center">
      <Modal
        visible={visible}
        width="50%"
        height="50%"
        effect="fadeInUp"
        onClickAway={() => onClose()}
      >
        <div>
          <br />
          <h1>{invitation.length > 0 && invitation[0].name}</h1>
          <h4>{invitation.length > 0 && invitation[0].from}</h4>
          <p>Kami mengundang Bapak/Ibu/Saudara/i ke acara pernikahan Kami.</p>
          <p>Terima kasih</p>
          <h1>
            <i class="ti-envelope size-lg"></i>
          </h1>
          <a href="#home" onClick={() => onClose()}>
            Buka undangan
          </a>
        </div>
      </Modal>
    </section>
  );
};

export default Modal2;
