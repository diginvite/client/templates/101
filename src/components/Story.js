import React from "react";
import Moment from "react-moment";

const Story = (props) => {
  return (
    <section className="story-section section-padding p-t-0">
      <div className="container">
        <div className="row">
          <div className="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
            <div className="section-title">
              <h2>Cerita Kami</h2>
              {/* <p>Recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat</p> */}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col col-lg-10 col-lg-offset-1">
            <div className="story-block">
              <ul>
                {props.data &&
                  props.data.detail.stories.map((data, i) => {
                    return (
                      <li>
                        {i % 2 !== 0 ? (
                          <div className="img-holder">
                            <img
                              src="http://themegeniuslab.com/html/jolly/assets/images/story/img-4.png"
                              alt="image_story"
                            />
                          </div>
                        ) : null}
                        <div className="details">
                          <h3>{data.title}</h3>
                          <span className="date">
                            {/* 25 Dec 2018 */}
                            <Moment format="DD MMM. YYYY">{data.date}</Moment>
                          </span>
                          <p>{data.description}</p>
                        </div>
                        {i % 2 === 0 ? (
                          <div className="img-holder">
                            <img
                              src="http://themegeniuslab.com/html/jolly/assets/images/story/img-4.png"
                              alt="image_story"
                            />
                          </div>
                        ) : null}
                      </li>
                    );
                  })}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Story;
