import React from 'react';

const Preload = (props) => {
  return(
    <div className="preloader">
        <div className="middle">
            <i className="fi flaticon-favorite-heart-button"></i>
            <i className="fi flaticon-favorite-heart-button"></i>
            <i className="fi flaticon-favorite-heart-button"></i>
            <i className="fi flaticon-favorite-heart-button"></i>
        </div>
    </div>
  )
}

export default Preload;