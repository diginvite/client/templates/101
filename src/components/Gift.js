import React from "react";
import Modal from "react-awesome-modal";

export default ({ data }) => {
  return (
    <section className="text-center">
      <Modal
        visible={true}
        width="80%"
        height="80%"
        effect="fadeInUp"
        // onClickAway={() => data.onClose()}
      >
        <div>
          <br />
          <h1>Give a Gift</h1>
          {/* <p>We invited you to celebrate our wedding</p> */}
          <h1>
            <i class="ti-gift size-lg"></i>
          </h1>
          <div className="row">
            {data.detail.payments.map((payment) => {
              return <div className="col-md-3"></div>;
            })}
          </div>

          {/* <a href="javascript:void(0);" onClick={() => props.onClose()}>
            Open
          </a> */}
        </div>
      </Modal>
    </section>
  );
};
